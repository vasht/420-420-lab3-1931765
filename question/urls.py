# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 12:01:10 2021

@author: Vashti
"""
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
app_name = 'question'

#sets the path for the view
urlpatterns = [
    path('',views.IndexView.as_view() , name='index'),
    path('<int:pk>/', views.DetailView.as_view() , name='detail'), # ex: /question/5/    
    path('<int:pk>/results/' ,views.ResultsView.as_view() ,name='results'), # ex: /question/5/results/   
    path('<int:question_id>/vote/', views.vote,name='vote'),     # ex: /question/5/vote/
    path('signup_view/', views.signup_view, name="signup_view"),
    path( 'login/', auth_views.LoginView.as_view(template_name='question/login.html'), name='login'),
    path( 'logout/', auth_views.LogoutView.as_view(template_name='question/logout.html'), name='logout'),
    path('question_create', views.QuestionCreate.as_view(template_name='question/create_question.html'), name='question_create' ),
    path('<int:pk>/delete', views.QuestionDeleteView.as_view(template_name='question/confirm_delete.html'), name='delete' ),
    path('<int:pk>/update', views.QuestionUpdate.as_view(), name='update' ),

    ]
