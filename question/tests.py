from django.test import TestCase

# Create your tests here.
import datetime
from django.test import TestCase
from django.utils import timezone
from .models import Question, Choice

def create_question(question_text, days):
   #Create a question with the given `question_text` and published the
   #given number of `days` offset to now (negative for questions published
   # in the past, positive for questions that have yet to be published).  
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class ChoiceModelTests(TestCase):
    #This test will check if the choice object's isisAvalidQuestion()
    #returns false if question_id is invalid (< 0)
    def test_choice_with_bad_questionID(self):
        '''
        This will create a question with an invalid  question_id 
        by setting the value with a negative number
        '''
        question = Question(question_id=-123)
        
        #Expected: isisAvalidQuestion() returnsFalse 
        #since question_id is set to be negative
        choice = Choice.create(question=question)  
        self.assertIs(choice.isisAvalidQuestion(), False) 
    
    #This test will check if the choice model accepts an invalid
    #votes number (ie negative).
    def test_choice_with_bad_vote_number(self):
        question = Question(question_id=1)
        choice = Choice(question=question,votes = -123)
        
        choice_has_bad_vote_number = choice.votes < 0
        self.assertIs(choice_has_bad_vote_number, True)

class QuestionModelTests(TestCase):
    def test_was_published_recently_with_future_question(self):
	#was_published_recently() returns False for questions whose pub_date
        	#is in the future.
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)
        
    def test_was_published_recently_with_old_question(self):
    	#was_published_recently() returns False for questions whose 	
        #pub_date is older than 1 day.
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)
        
    def test_was_published_recently_with_recent_question(self):
        #was_published_recently() returns True for questions whose 	
        #pub_date is within the last day.
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)
        




from django.urls import reverse
class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        #The detail view of a question with a pub_date in the future returns a 404 not found.
        future_question = create_question(question_text='Future question.', days=5)
        url = reverse('question:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
       # The detail view of a question with a pub_date in the past   displays the question's text.
        past_question = create_question(question_text='Past Question.', days=-5)
        url = reverse('question:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


