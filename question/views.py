from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Question
from django.http import Http404

from django.http import HttpResponseRedirect
from .models import Choice
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from django.views import generic

from django.utils import timezone

class QuestionUpdate(UpdateView):
    model = Question
    fields = ['question_text', 'pub_date', 'image']
    template_name = 'question/update_question.html'
    success_url ="/"


class QuestionCreate(CreateView):
  
    # specify the model for create view
    model = Question
  
    # specify the fields to be displayed
  
    fields = ['question_text', 'pub_date', 'image']
    
    success_url ="/"

class QuestionDeleteView(DeleteView):
    # specify the model you want to use
    model = Question
      
    # can specify success url
    # url to redirect after successfully
    # deleting object
    success_url ="/"


class IndexView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    template_name= 'question/index.html'
    context_object_name= 'latest_question_list'
    # def get_queryset(self):
    #     #Return the last five published questions.
    #     return Question.objects.order_by('-pub_date')[:5]
    def get_queryset(self):
        #Excludes any questions that aren't published yet.
        return Question.objects.filter(pub_date__lte=timezone.now())

class DetailView(generic.DetailView):
    model = Question
    template_name= 'question/detail.html'
    
    
class ResultsView(generic.DetailView):
    model = Question
    template_name= 'question/results.html'



def index(request):
    '''
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)
    #return HttpResponse("Hello, world. You're at the polls???(should be question tho) index.")
    '''
    latest_question_list = Question.objects.order_by('-pub_date')[:2]
    #output = ', '.join([q.question_text for q in latest_question_list])
    #return HttpResponse(output)
    template = loader.get_template('question/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))



def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
        
    return render(request,'question/detail.html',{'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'question/results.html', {'question': question})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice= question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'question/detail.html', 
                      {'question': question,'error_message': "You didn't select a choice.",})
    else:
        selected_choice.votes+= 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('question:results', args=(question.id,)))


from .forms import SignUpForm
from django.contrib.auth import login, authenticate
from django.shortcuts import redirect

def signup_view(request):
    form = SignUpForm(request.POST)
    print( 'form', form )
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('question:index')
    return render(request, 'question/signup.html', {'form': form})


