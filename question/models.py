from django.db import models

from django.utils import timezone
import datetime
# Create your models here.

def user_directory_path(instance, filename):
    #file will be uploaded to items_images/user_<id>/<filename>
    return 'items_images/user_{0}/{1}'.format(instance.id, filename)


class Question(models.Model):
    question_text = models.CharField(max_length = 200)
    pub_date = models.DateTimeField('date published')
    #image = models.ImageField(upload_to = user_directory_path)
    question_id = models.IntegerField(default=0)
    def __str__(self):
            return self.question_text
    def was_published_recently(self):
        now = timezone.now() 
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    
class Choice(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text
    
    def isAvalidQuestion(self):
        return self.question.question_id < 0
    
class User(models.Model):
    choice =   models.ForeignKey(Choice, on_delete=models.CASCADE)
    user_name =  models.CharField(max_length=200)
    def __str__(self):
        return self.user_name


